#!/usr/bin/env python3

#depends on ffmpeg-python

import os
import sys
import ffmpeg
import numpy as np

class FFMPEGWriter:

	def __init__(self, fname, width, height, fps, vcodec=None):
		#vcodec='libvpx'
		output_params = {
			'pix_fmt':'yuv420p',
			'r': fps,
			'b:v': '800k',
		}
		if os.path.splitext(fname)[-1].lower() == '.webm':
			if vcodec is None:
				vcodec = 'libvpx'
				output_params['crf'] = 30
		if vcodec:
			output_params['vcodec'] = vcodec
#		print('PARAMS', output_params, file=sys.stderr)
		self.process = (
			ffmpeg
				.input('pipe:', format='rawvideo', pix_fmt='rgb24', s='{}x{}'.format(width, height))
				.output(fname, **output_params)
				.overwrite_output()
				.run_async(pipe_stdin=True, quiet=True)
		)

	def write(self, frame):
		self.process.stdin.write(frame.astype(np.uint8).tobytes())

	def release(self):
		self.process.stdin.close()
		self.process.wait()


if __name__ == '__main__':

	import cv2
	import time

	def vidwrite(fn, image_generator, framerate=30):
		frame = next(image_generator)
		height,width,channels = frame.shape
#		writer = FFMPEGWriter(fn, width, height, framerate, vcodec=vcodec)
		writer = FFMPEGWriter(fn, width, height, framerate)
		try:
			while True:
				writer.write(frame)
				frame = next(image_generator)
		except StopIteration:
			pass
		writer.release()

	def make_video_generator(video_reader):
		while True:
			ret_val, image = video_reader.read()
#			print('Read frame', ret_val, file=sys.stderr)
			if not ret_val:
				break
			yield image

	def run(ifname, ofname):
		if not os.path.exists(ifname):
			print(ifname, 'not found', file=sys.stderr)
		else:
			print('Processing', ifname, file=sys.stderr)
		video_reader = cv2.VideoCapture(ifname)
#		print('READER', video_reader)
		nb_frames = int(video_reader.get(cv2.CAP_PROP_FRAME_COUNT))
		fps = int(video_reader.get(cv2.CAP_PROP_FPS))
#		print('FPS', fps, file=sys.stderr)
		image_generator = make_video_generator(video_reader)
		vidwrite(ofname, make_video_generator(video_reader), framerate=fps)

	run(sys.argv[1], sys.argv[2])

import os, glob, sys, time

def config_tf(memory_limit=1024):
	import tensorflow as tf

	gpus = tf.config.experimental.list_physical_devices('GPU')
	if not gpus:
		raise Exception('No GPU available, check the environment')

	# Currently, memory growth needs to be the same across GPUs
	try:
		for gpu in gpus:
			tf.config.experimental.set_memory_growth(gpu, True)
	except:
		print('Can not set memory growth')
	tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=memory_limit)])
	logical_gpus = tf.config.experimental.list_logical_devices('GPU')
	print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")

def dbprint(*args, **kwds):
	if 'file' not in kwds:
		kwds['file'] = sys.stdout
	if 'flush' not in kwds:
		kwds['flush'] = True
	print(time.strftime('%Y-%m-%d %H:%M:%S'), '[{:d}]'.format(os.getpid()), *args, **kwds)

def clear_dir(dname, extlist=('jpg', 'mp4', 'jpeg', 'png', 'json')):
	for fname in glob.glob(os.path.join(dname, '*')):
		if os.path.isdir(fname):
			clear_dir(fname)
			if not os.listdir(fname):
				os.rmdir(fname)
		elif os.path.splitext(fname)[-1].lstrip('.') in extlist:
			os.unlink(fname)

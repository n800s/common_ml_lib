#docker run --gpus=1 --rm -p8000:8000 -p8001:8001 -p8002:8002 -v/home/n800s/triton/triton_model_repository:/models \
#-v/home/n800s/triton/plugins:/plugins --env LD_PRELOAD=/plugins/liblayerplugin.so \
#nvcr.io/nvidia/tritonserver:21.09-py3 tritonserver --strict-model-config=false --model-repository=/models &>run_triton.log
docker run --gpus=1 --rm -p8000:8000 -p8001:8001 -p8002:8002 -v`pwd`/triton_model_repository:/models \
nvcr.io/nvidia/tritonserver:21.09-py3 tritonserver --log-verbose 1 --log-info 1 --model-repository=/models &>run_triton.log
echo $?


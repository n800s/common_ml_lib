#!/usr/bin/env python3

import numpy as np
import time
import sys
import os
import cv2
import importlib

import tritonclient.http as httpclient
import tritonclient.grpc as grpcclient
from tritonclient.utils import InferenceServerException
class ProcessFile:

	I_EXTS = ['.jpg', '.png']
	V_EXTS = ['.mp4', '.avi']

	def __init__(self, ifname, odname, model_name, vcodec='XVID'):
		self.ifname = ifname
		self.odname = odname
		self.model_name = model_name
		self.vcodec = vcodec
		self.triton_client = grpcclient.InferenceServerClient(FLAGS.url, verbose=FLAGS.verbose)
		self.metadata = self.triton_client.get_model_metadata(model_name)
		if len(self.metadata.inputs) != 1:
			raise Exception('Expect one model input only, %d found' % len(self.metadata.inputs))
		print(self.metadata,'input:', self.metadata.inputs[0].name, self.metadata.inputs[0].shape, 'output:', self.metadata.outputs[0].shape)
		ibatch, self.iheight, self.iwidth, self.channels = self.metadata.inputs[0].shape
		obatch, self.osize, self.classes = self.metadata.outputs[0].shape
		config = importlib.import_module(model_name)
		self.oheight,self.owidth = config.output_shape[:2]

	def infer(self, _input, input_li, headers=None):
		inputs = []
		outputs = []
		inputs.append(grpcclient.InferInput(self.metadata.inputs[0].name, input_li, "FP32"))
		inputs[0].set_data_from_numpy(_input)
		results = self.triton_client.infer(self.model_name,  inputs, outputs=outputs, headers=headers)
		return results

	def process_frame_data(self, frame_data):
		for frame in frame_data['frame_generator']:
			img = cv2.resize(frame, (self.iwidth, self.iheight))
#			print('img shape', img.shape)
			img = img.reshape((self.iheight, self.iwidth, self.channels))
#			print('img reshaped', img.shape)

			input_data = np.expand_dims(img, axis=0)
			input_data = input_data.astype(np.float32)
			batch, row, col, ch = input_data.shape
#			print('input data shape', input_data.shape, input_data.min(), input_data.max())

			results = self.infer(input_data, [batch, row, col, ch])

#			statistics = self.triton_client.get_inference_statistics(model_name=model_name, headers=headers_dict)
#			print(statistics)
#			if len(statistics['model_stats']) != 1:
#				print("FAILED: Inference Statistics")
#				sys.exit(1)

#			print('RESULTS', vars(results))
			results = results.as_numpy(self.metadata.outputs[0].name)
#			print('output shape:', results.shape)

			prediction = np.frombuffer(results, np.float32)

			prediction = prediction.reshape((self.oheight, self.owidth, self.classes))
#			print('prediction:', prediction.shape, prediction.min(), prediction.max())

			i_prediction = prediction.argmax(axis=2)
			clr = [
				(255,0,0), (229,176,115), (29,115,29), (0,34,51),
				(116,0,217), (229,0,0), (51,34,0), (0,255,34),
				(64,191,255), (88,57,115), (217,0,0), (255,204,0),
				(108,217,123), (0,61,115), (51,26,49), (51,26,26),
				(102,82,0), (134,179,152), (0,87,217), (217,0,173),
				(102,14,0), (230,226,172), (0,102,82), (191,200,255),
				(230,172,218), (242,137,121), (204,255,0),
				(64,255,217), (7,0,51), (102,0,68), (255,208,191),
				(207,230,115), (57,218,230), (90,86,115),
				(115,86,98), (115,63,29), (109,115,86), (38,50,51),
				(31,0,115), (242,61,109), (255,136,0), (29,51,26),
				(0,92,115), (170,121,242), (140,70,79)
			]
			oimg = cv2.resize(frame, (self.owidth, self.oheight))
			if len(oimg.shape) == 2:
#				oimg = oimg.reshape((self.oheight, self.owidth, 1))
				oimg = cv2.cvtColor(oimg, cv2.COLOR_GRAY2RGB)
			for i in range(1, 6, 1):
				oimg[i_prediction==i] = clr[i]
			oimg = cv2.cvtColor(oimg, cv2.COLOR_RGB2BGR)

#			print('i_prediction:', i_prediction.shape, i_prediction.min(), i_prediction.max(), np.unique(i_prediction, return_counts=True))
			yield {
				'preview_image': oimg,
				'prediction': i_prediction,
			}

	def run(self):
		bname,bext = os.path.splitext(os.path.basename(self.ifname))
		print('\n%s' % self.ifname)
		frame_data = None
		if bext in self.V_EXTS:
			video_mode = True
			start_ts = time.time()
			video_reader = cv2.VideoCapture(self.ifname)
			nb_frames = int(video_reader.get(cv2.CAP_PROP_FRAME_COUNT))
			fps = int(video_reader.get(cv2.CAP_PROP_FPS))
			frame_data = {'frame_generator': self.make_video_generator(video_reader), 'fps': fps}
		elif bext in self.I_EXTS:
			video_mode = False
			img = cv2.imread(self.ifname)
			img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY if self.channels == 1 else cv2.COLOR_BGR2RGB)
			frame_data = {'frame_generator': (img,)}
		if not frame_data is None:
			video_writer = None
			json_data = {'frames': []}
			suffix = ''
			for i,result in enumerate(self.process_frame_data(frame_data)):
				if video_mode:
					if video_writer is None:
						frame_h,frame_w = result['preview_image'].shape[:2]
						video_writer = cv2.VideoWriter(os.path.join(self.odname, bname + '_preview' + '.mp4'), cv2.VideoWriter_fourcc(*'mp4v'), fps, (frame_w, frame_h))
					video_writer.write(result['preview_image'])
				else:
					cv2.imwrite(os.path.join(self.odname, bname + '_preview' + '.png'), result['preview_image'])
			if not video_writer is None:
				video_writer.release()
				dt = time.time() - start_ts
				print('Processed %d frames for %.2f sec (%.2f fps)' % (nb_frames, dt, nb_frames/dt))

	def make_video_generator(self, video_reader):
		while True:
			ret_val, image = video_reader.read()
			if not ret_val:
				break
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY if self.channels == 1 else cv2.COLOR_BGR2RGB)
			yield image

if __name__ == '__main__':

	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('-v',
						'--verbose',
						action="store_true",
						required=False,
						default=False,
						help='Enable verbose output')
	parser.add_argument('-u',
						'--url',
						type=str,
						required=False,
						default='localhost:8001',
						help='Inference server URL. Default is localhost:8001.')
	parser.add_argument('-m',
						'--model_name',
						type=str,
						default="dumpster_unet",
						help='Segmentation model name')
	parser.add_argument('-i',
						'--input',
						type=str,
						required=True,
						help='Image File')
	parser.add_argument('-o',
						'--odir',
						type=str,
						required=True,
						help='Output Directory')

	FLAGS = parser.parse_args()

	ProcessFile(FLAGS.input, FLAGS.odir, FLAGS.model_name).run()


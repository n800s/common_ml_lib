IDIR=drobilka_test
MODEL_NAME=drobilka_pspnet_50
#IDIR=dumpster_test
#MODEL_NAME=dumpster_unet
#MODEL_NAME=dumpster_pspnet
ODIR=output_$MODEL_NAME
LOG=image_segmentation_$MODEL_NAME.log
rm $ODIR/*.png
mkdir $ODIR
echo > $LOG
for fname in `find "$IDIR" -name '*.mp4' -o -name '*.jpg'`
do
#	echo IFNAME=$fname
	python3 image_segmentation.py -u localhost:8001 --input ${fname} --odir ${ODIR} --model_name=$MODEL_NAME &>> ${LOG}
	if [ $? != 0 ]
	then
		break
	fi
done

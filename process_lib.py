import sys, os, subprocess, shutil, argparse, json, redis, tempfile, time, traceback, urllib, glob, base64
from itertools import chain
import wget
from multiprocessing import Process, Queue
from queue import Empty
from ml_utils import dbprint

def uri2fname(uri, tmpdir):
	uri_obj = urllib.parse.urlparse(uri)
	if uri_obj.scheme == 'file':
		fname = uri_obj.path
	else:
		fname = wget.download(uri, out=tmpdir, bar=None)
	return fname

def process_name(suffix):
	return '{}_{}'.format(os.path.basename(sys.argv[0]), suffix)

class ProcessLib:

	def __init__(self, process_prefix='dummy', redis_prefix='dummy'):
		self.debug = False
		self.debug_path = os.path.join(os.path.dirname(sys.argv[0]), 'debug')
		self.uploader_process = None
		self.process_prefix = process_prefix
		self.redis_prefix = redis_prefix
		self.tmp_path = os.environ.get('TMP_PATH', os.path.expanduser(f'~/tmp/{process_prefix}/'))
		if not os.path.exists(self.tmp_path):
			os.makedirs(self.tmp_path)
		# a process for each algorithm
		self.predictor_process_map = {}
		self.uploader_process = None

	def start(self):
		ap = argparse.ArgumentParser()
		ap.add_argument('--redis_host', type=str, default='localhost')
		args = vars(ap.parse_args())
		self.r = redis.Redis(host=args['redis_host'])
		self.uploader_q = Queue()
		dbprint('Started')

	def check_uploader(self):
		if self.uploader_process is None or not self.uploader_process.is_alive():
			self.uploader_process = Process(target=self.uploader, args=(self.uploader_q,), name=process_name('uploader'))
			self.uploader_process.start()

	def get_q4alg(self, alg):
		if alg not in self.predictor_process_map:
			self.predictor_process_map[alg] = self.create_process_dict(alg, self.predictor, alg)
#			dbprint('PROCESS CREATED', self.predictor_process_map[alg]['process'])
		self.check_process_dict(self.predictor_process_map[alg])
		return self.predictor_process_map[alg]['child_q'], self.predictor_process_map[alg]['parent_q']

	def uploader(self, q):
		while True:
			try:
				item = q.get()
				dbprint('item:{}'.format(item))
				if item.get('quit', False):
					break
				dbprint('Executing: %s' % ' '.join(item['cmd']))
				subprocess.check_output(item['cmd'])
				self.clean_unneeded(item.get('filelist', []), [item['tmpdir']] if 'tmpdir' in item else [])
			except Exception as e:
				dbprint(e)

	def create_process_dict(self, suffix, target, *args):
		rs = {
			'suffix': suffix,
			'target': target,
			'args': args,
			'child_q': Queue(),
			'parent_q': Queue(),
			'process': None,
		}
		return self.check_process_dict(rs)

	def check_process_dict(self, pr):
		if pr['process'] is None or not pr['process'].is_alive():
			pr['process'] = Process(target=pr['target'], args=(*pr['args'], pr['parent_q'], pr['child_q']), name=process_name(pr['suffix']))
			pr['process'].start()
		return pr

	def clean_unneeded(self, filelist, dirlist):
		self.update_debug()
		if not self.debug:
			for fname in filelist:
				if os.path.exists(fname):
					dbprint('Removing %s' % (fname,))
					os.unlink(fname)
			for dname in dirlist:
				if os.path.exists(dname):
					dbprint('Removing directory %s' % (dname,))
					shutil.rmtree(dname)

	def redis_callback(self, callback_uri, data, callback_files=None, tmpdirs=[]):
		uri_parts = urllib.parse.urlparse(callback_uri)
		params = {
			'host': uri_parts.hostname,
		}
		if uri_parts.port:
			params['port'] = uri_parts.port
		dbprint('Callback redis destination: {}:{} {}'.format(uri_parts.hostname, uri_parts.port if uri_parts.port else 'default', uri_parts.path))
		r = redis.Redis(**params)
		redis_parts = uri_parts.path.split('/')
		if not callback_files is None:
			data['files'] = {}
			for fname in callback_files:
				data['files'][os.path.basename(fname)] = base64.b64encode(open(fname, 'rb').read()).decode()
		if len(redis_parts) >= 3:
			rhash,rkey = redis_parts[1:3]
			r.hset(rhash, rkey, json.dumps(data, default=str))
		else:
			rlist = redis_parts[1]
			r.rpush(rlist, json.dumps(data, default=str))
		self.clean_unneeded(callback_files if callback_files else [], tmpdirs)

	def update_debug(self):
		self.debug = os.path.exists(self.debug_path)

	def run(self):
		self.check_uploader()
		while True:
			try:
				qname = 'l.{}_request'.format(self.redis_prefix)
				dbprint('Waiting for request in {}'.format(qname))
				req = self.r.brpop(qname, 0)[1].decode("utf-8")
				start_ts = time.time()
#				dbprint('Found {}'.format(req))
				# write the last request processing timestamp to redis
				self.r.set('l.{}_last_ts'.format(self.redis_prefix), str(int(time.time())))
				req = json.loads(req)
				callback_uri = req['callback_uri']
				dbprint('Found with callback uri {}'.format(callback_uri))
				codec = 'VP80'
				try:
					self.update_debug()
					if self.debug:
						dbprint('request: %s' % json.dumps(req, indent=2, default=str))
					req_debug = req.get('get_debug', False)
					tmpdir = tempfile.mkdtemp(prefix='wget_', dir=self.tmp_path)
					media_obj = req.get('media_obj', None)
					media_b64 = req.get('media_b64', None)
					if media_obj is None and media_b64 is None:
						media_hkey = req.get('media_hkey', None)
						if media_hkey is None:
							media_key = req.get('media_key', None)
							if media_key is None:
								media_uri = req.get('media_uri', None)
								if not media_uri:
									media_uri = req.get('uri', None)
								dbprint('Reading ... %s' % media_uri)
								uri_obj = urllib.parse.urlparse(media_uri)
								if uri_obj.scheme == 'file':
									fname = uri_obj.path
								else:
									fname = wget.download(media_uri, out=tmpdir, bar=None)
							else:
								dbprint('req media key obj size: %s' % r.strlen(media_key))
								f = open(os.path.join(tmpdir, media_key), 'wb')
								f.write(r.get(media_key))
								f.flush()
								fname = f.name
						else:
							f = open(os.path.join(tmpdir, media_hkey), 'wb')
							f.write(r.hget(req['media_hash'], media_hkey))
							f.flush()
							fname = f.name
						dbprint('Retrieved as %s' % fname)
					else:
						if media_obj is None:
							dbprint('req media b64 size: %s' % len(media_b64))
							media_obj = base64.b64decode(media_b64)
						else:
							media_obj = media_obj.encode('latin1')
						dbprint('req media obj size: %s' % len(media_obj))
						f = tempfile.NamedTemporaryFile(dir=tmpdir, suffix='.png', delete=False)
						f.write(media_obj)
						f.flush()
						fname = f.name
					dbprint('Processing %s' % fname)
					start_ts = time.time()
					bname = os.path.splitext(os.path.basename(fname))[0]
					ofname = None
					ofname_montage = None
					ofname = os.path.join(tmpdir, bname + '.output')
					ofname_montage = os.path.join(tmpdir, bname + '.montage')
					alg = req.get('algorithm', 'default')
					idata = {
						'fname': fname,
						'odir': tmpdir,
						'ofname': ofname,
						'ofname_montage': ofname_montage,
						'vcodec': codec,
						'debug': self.debug or req_debug,
						'ttype': req.get('ttype', None),
						'algorithm': alg,
						'reply': '{}_reply_'.format(self.redis_prefix) + bname,
					}
					child_q,parent_q = self.get_q4alg(alg)
#					dbprint('PUT', idata, 'TO', child_q)
					child_q.put(idata)
					data = None
					while True:
						try:
							data = parent_q.get(True, 10)
							break
						except Empty:
							if not self.predictor_process_map[alg]['process'].is_alive():
								self.predictor_process_map[alg]['process'].join()
								raise Exception('The predictor for {} is dead!!! Long live the predictor!!!'.format(alg))
					if isinstance(data, (bytes, str)):
						data = json.loads(data)
					if 'error' in data:
						req['error'] = data['error']
						self.r.lpush('l.{}_errors'.format(self.redis_prefix), json.dumps(req, default=str))
						if callback_uri.startswith('redis://'):
							self.redis_callback(callback_uri, {'error': data['error']})
						clean_unneeded([], [tmpdir])
					else:
						if 'userdata' in req:
							data['userdata'] = req['userdata']
						callback_files = []
						json_fname = fname + '.json'
						json.dump(data, open(json_fname, 'w'), indent=4, default=str)
#						callback_files.append(json_fname)
						get_files = [os.path.basename(gn) for gn in req.get('get_files', ['*'])]
						dbprint('get_files:', get_files)
						callback_files = list(set(chain(*[glob.glob(os.path.join(tmpdir, gn)) for gn in get_files])))
						print('callback_files:', callback_files)
						if callback_files:
							total_size = 0
							if callback_uri.startswith('redis://'):
								self.redis_callback(callback_uri, data, callback_files if req.get('add_files_to_json', False) else None, [tmpdir])
							else:
								if callback_uri.startswith('file://'):
									cmd = ['cp', '-r']
									for fname in callback_files:
										cmd.append(fname)
										total_size += os.path.getsize(fname)
									cmd.append(urllib.parse.urlparse(callback_uri).path)
								else:
									cmd = ['curl']
									for fname in callback_files:
										cmd +=  ['-F', 'file[]=@{};filename={}'.format(fname, os.path.basename(fname))]
										total_size += os.path.getsize(fname)
									cmd.append(callback_uri)
								dbprint("Total upload file size: {}".format(total_size))
								item = {'cmd': cmd}
								if not self.debug:
									item['filelist'] = callback_files
									item['tmpdir'] = tmpdir
								self.uploader_q.put(item)
					dbprint('Processed in {:.2f} secs'.format(time.time() - start_ts))
				except KeyboardInterrupt:
					raise
				except:
					req['error'] = traceback.format_exc()
					dbprint('Error:', req['error'])
					self.r.lpush('l.{}_errors'.format(self.redis_prefix), json.dumps(req, default=str))
			except KeyboardInterrupt:
				self.uploader_q.put({'quit': True})
				p.join()
				raise
			except:
				dbprint('Error:', traceback.format_exc())
				pass

	def predictor(self, alg, parent_q, child_q):
		dbprint('Starting predictor process...')
		predict_process = {}
		while True:
			try:
				item = child_q.get()
				dbprint('item:{}'.format(json.dumps(item, indent=2, default=str)))
				if item.get('quit', False):
					for p in predict_process.values():
						p['child_q'].put(item)
						p['process'].join()
					break
				odata = self.predict_item(item)
				if self.debug:
					dbprint('PROCESSED DATA:', json.dumps(odata, indent=2, default=str))
				json.dump(odata, open(item['ofname'] + '.json', 'wt'), indent=2, default=str)
				parent_q.put(odata)
			except Exception as e:
				parent_q.put({'error': str(e)})
				dbprint(alg.upper(), 'ERROR:', e)
				traceback.print_exc(file=open(os.path.join(self.tmp_path, 'last_error'), 'a'))

	# must be overwritten
	def predict_item(self, item):
		return {}

# import the necessary packages
import numpy as np
import cv2

def imgreduce(img, max_width, max_height):
	return imgresize(img, max_width, max_height) if img.shape[0] > max_height or img.shape[1] > max_width else img

def imgresize(img, width, height):
	image_height,image_width = img.shape[:2]
	aspect_ratio = image_width / image_height
#	print('ASPECT', aspect_ratio)
	if aspect_ratio < 1:
		width *= aspect_ratio
	elif aspect_ratio > 1:
		height /= aspect_ratio
#	print('NEW SIZE', width, height)
	return cv2.resize(img, (int(width), int(height)), interpolation=cv2.INTER_AREA)

class ResultsMontage:
	def __init__(self, imageSize, imagesPerRow, numResults, depth=4):
		# store the target image size and the number of images per row
		self.imageW = imageSize[0]
		self.imageH = imageSize[1]
		self.imagesPerRow = imagesPerRow
		self.numResults = numResults

		# allocate memory for the output image
		numCols = numResults // imagesPerRow
		self.montage = np.zeros((numCols * self.imageW, imagesPerRow * self.imageH, depth), dtype="uint8")

		# initialize the counter for the current image along with the row and column
		# number
		self.counter = 0
		self.row = 0
		self.col = 0

	def addResult(self, image, text=None, highlight=False):

		if self.counter < self.numResults:

			# check to see if the number of images per row has been met, and if so, reset
			# the column counter and increment the row
			if self.counter != 0 and self.counter % self.imagesPerRow == 0:
				self.col = 0
				self.row += 1

			# resize the image to the fixed width and height and set it in the montage
			if image.shape[0] > self.imageH or image.shape[1] > self.imageW:
				image = cv2.resize(image, (self.imageH, self.imageW))
			(startY, endY) = (self.row * self.imageW, self.row * self.imageW + image.shape[0])
			(startX, endX) = (self.col * self.imageH, self.col * self.imageH + image.shape[1])
			if len(image.shape) == 2:
				image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGRA)
			self.montage[startY:endY, startX:endX] = image

			# if the text is not None, draw it
			if text is not None:
				cv2.putText(self.montage, text, (startX + 10, startY + 30), cv2.FONT_HERSHEY_SIMPLEX,
					1.0, (0, 255, 255), 3)

			# check to see if the result should be highlighted
			if highlight:
				cv2.rectangle(self.montage, (startX + 3, startY + 3), (endX - 3, endY - 3), (0, 255, 0), 4)

			# increment the column counter and image counter
			self.col += 1
			self.counter += 1

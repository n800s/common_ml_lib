import sys, os, atexit, time, json, random, tempfile, base64, urllib, subprocess, traceback, shutil, signal, glob
import redis
import wget
import psutil
from queue import Empty
from multiprocessing import Process, Queue
from itertools import chain
from .ml_utils import dbprint

IMG_EXT = ('.jpg', '.png', '.jpeg')
LAST_ERROR = '~/tmp/last_error'

TMP_PATH = os.environ['TMP_PATH']
OUTPUT_VEXT = '.webm'
OUTPUT_CODEC = 'VP80'
#OUTPUT_VEXT = '.avi'
#OUTPUT_CODEC = 'XVID'
QUEUE_TIMEOUT = 10

class predictorLoop:

	def __init__(self, redis_host=os.environ.get('REDIS_HOST', 'localhost'), appname=os.environ['APPNAME']):
		# temp files handles to prevent premature deleting
		self.pp = None
		self.uploader_p = None
		self.appname = appname
		self.pid_fname = os.environ.get('PID_FNAME', os.path.join(os.path.dirname(__file__), self.appname + '.pid'))
		self.finish = False
		self.tmpfiles = []
		self.debug = False
		self.debug_path = os.path.join(os.path.dirname(sys.argv[0]), 'debug')
		self.req_debug = False
		self.uploader_q = Queue()
		self.parent_q = Queue()
		self.child_q = Queue()
		self.r = redis.Redis(host=redis_host)

	def __del__(self):
		if self.pp:
			self.pp.terminate()
		if self.uploader_p:
			self.uploader_p.terminate()
		dbprint('Processes terminated')

	def update_debug(self):
		self.debug = os.path.exists(self.debug_path)

	def remove_pid(self):
		if os.path.exists(self.pid_fname):
			os.unlink(self.pid_fname)
			dbprint(self.pid_fname, 'removed')

	def init(self):
		atexit.register(self.remove_pid)
		# writing pid
		print(os.getpid(), file=open(self.pid_fname, 'w'))
		self.uploader_p = Process(target=self.uploader, args=(os.getpid(), self.uploader_q,))
		self.pp = Process(target=self.predictor, args=(os.getpid(), self.parent_q, self.child_q))
		signal.signal(signal.SIGTERM, self.terminate_gracefully)

	def skip_request(self, req):
		return False

	def req_postprocess(self, reply):
		pass

	def clean_unneeded(self, filelist, dirlist):
		self.update_debug()
		if not self.debug:
			for fname in filelist:
				if os.path.exists(fname):
					dbprint('Removing %s' % (fname,))
					os.unlink(fname)
			for dname in dirlist:
				if os.path.exists(dname):
					dbprint('Removing directory %s' % (dname,))
					shutil.rmtree(dname)

	# subprocess
	def uploader(self, parent_pid, q):
		i = 1
		waiting_message = True
		while psutil.pid_exists(parent_pid):
			try:
				if waiting_message:
					dbprint('Uploader waiting ({}) ...'.format(i))
				waiting_message = False
				i += 1
				item = q.get(True, QUEUE_TIMEOUT)
				waiting_message = True
				dbprint('item:{}'.format(item))
				if item.get('quit', False):
					break
				self.debug = item['debug']
				dbprint('Executing: %s' % ' '.join(item['cmd']))
				subprocess.check_output(item['cmd'])
				self.clean_unneeded(item.get('filelist', []), [item['tmpdir']] if 'tmpdir' in item else [])
			except Empty:
				continue
			except Exception as e:
				dbprint('Error: {} {}'.format(type(e), e))
		dbprint('Uploader is exiting')

	def redis_callback(self, callback_uri, data, callback_files=None, tmpdirs=[]):
		uri_parts = urllib.parse.urlparse(callback_uri)
		params = {
			'host': uri_parts.hostname,
		}
		if uri_parts.port:
			params['port'] = uri_parts.port
		dbprint('Callback redis destination: {}:{} {}'.format(uri_parts.hostname, uri_parts.port if uri_parts.port else 'default', uri_parts.path))
		r = redis.Redis(**params)
		redis_parts = uri_parts.path.split('/')
		if not callback_files is None:
			data['files'] = {}
			for fname in callback_files:
				data['files'][os.path.basename(fname)] = base64.b64encode(open(fname, 'rb').read()).decode()
		if len(redis_parts) >= 3:
			rhash,rkey = redis_parts[1:3]
			r.hset(rhash, rkey, json.dumps(data, default=str))
		else:
			rlist = redis_parts[1]
			r.rpush(rlist, json.dumps(data, default=str))
		self.clean_unneeded(callback_files if callback_files else [], tmpdirs)

	def wait4request(self):
		fnames = []
		tmpdir = None
		waiting_message = True
#		dbprint('wait4request begins')
		while not self.finish:
			self.debug = os.path.exists(os.path.join(os.path.dirname(__file__), 'debug'))
			if waiting_message:
				dbprint('Waiting for redis request ...')
			waiting_message = False
			req = self.r.brpop(os.environ['REDIS_REQUEST_QUEUE'], QUEUE_TIMEOUT)
			if not req:
				continue
			waiting_message = True
			req = req[1].decode("utf-8")
			req = json.loads(req)
			if self.skip_request(req):
				continue
			else:
				req['start_ms'] = int(time.time() * 1000)
				if not os.path.exists(TMP_PATH):
					os.makedirs(TMP_PATH)
				tmpdir = tempfile.mkdtemp(prefix='wget_', dir=TMP_PATH)
				self.update_debug()
				if self.debug:
					dbprint('request: %s' % json.dumps(req, indent=4))
				self.req_debug = req.get('get_debug', False)
				media_obj = req.get('media_obj', None)
				media_b64 = req.get('media_b64', None)
				if media_obj is None and media_b64 is None:
					media_hkey = req.get('media_hkey', None)
					if media_hkey is None:
						media_key = req.get('media_key', None)
						if media_key is None:
							media_uri = req.get('media_uri', None)
							if not media_uri:
								media_uri = req.get('uri', None)
							dbprint('Reading ... %s' % media_uri)
							uri_obj = urllib.parse.urlparse(media_uri)
							if uri_obj.scheme == 'file':
								fname = uri_obj.path
							elif uri_obj.scheme == 'redis':
								media_r = redis.Redis(host=uri_obj.hostname, port=uri_obj.port if uri_obj.port else 6379)
								path_parts = uri_obj.path.split('/')[1:3]
								if len(path_parts) == 2:
									rhash,rkey = path_parts
									media_obj = media_self.r.hget(rhash, rkey)
								else:
									rkey = path_parts[0]
									media_obj = media_self.r.get(rkey)
								if media_obj is None:
									raise Exception('No media data found in redis ({})'.format(media_uri))
								_,iext = os.path.splitext(rkey)
								fname = rkey
								if iext not in IMG_EXT:
									fname += '.png'
								f = open(os.path.join(tmpdir, fname), 'wb')
								f.write(media_obj)
								f.flush()
								fname = f.name
							else:
								fname = wget.download(media_uri, out=tmpdir, bar=None)
						else:
							dbprint('req media key obj size: %s' % self.r.strlen(media_key))
							f = open(os.path.join(tmpdir, media_key), 'wb')
							f.write(self.r.get(media_key))
							f.flush()
							fname = f.name
					else:
						f = open(os.path.join(tmpdir, media_hkey), 'wb')
						f.write(self.r.hget(req['media_hash'], media_hkey))
						f.flush()
						fname = f.name
				else:
					if media_obj is None:
						dbprint('req media b64 size: %s' % len(media_b64))
						media_obj = base64.b64decode(media_b64)
					else:
						media_obj = media_obj.encode('latin1')
					dbprint('req media obj size: %s' % len(media_obj))
					f = tempfile.NamedTemporaryFile(dir=tmpdir, suffix='.png', prefix=req.get('callback_filename_prefix', 'namedtmp') + '_', delete=False)
					self.tmpfiles.append(f)
					f.write(media_obj)
					f.flush()
					fname = f.name
				fnames.append(fname)
				break
#		dbprint('wait4request is done', fnames)
		return req,fnames,tmpdir

	def process_request(self, req, fnames, tmpdir):
		callback_uri = req['callback_uri']
		for fname in fnames:
			dbprint('Processing %s' % fname)
			bname = os.path.splitext(fname)[0]
			idata = {
				'args': [
					fname,
					tmpdir,
				],
				'kwds': {
					'vcodec': OUTPUT_CODEC,
					'debug': self.debug or self.req_debug,
					'ttype': req.get('ttype', None),
					'algorithm': req.get('algorithm', 'default'),
				},
				'debug': self.debug,
				'reply': 'wagon_number_reply_' + bname,
			}
			start_predicting_ms = int(time.time() * 1000)
			self.child_q.put(idata)
			while not self.finish:
				try:
					data = self.parent_q.get(True, QUEUE_TIMEOUT)
				except Empty:
					data = None
					if not self.pp.is_alive():
						self.pp.join()
						raise Exception('The predictor is dead!!! Long live the predictor!!!')
				if data:
					dbprint('PROCESSED DATA:', data)
					if 'error' in data:
						req['error'] = data['error']
						self.r.lpush(os.environ['REDIS_ERROR_QUEUE'], json.dumps(req))
						if callback_uri.startswith('redis://'):
							self.redis_callback(callback_uri, {'error': data['error']})
					else:
						data['predict_stat'] = {
							'start_ms': req['start_ms'],
							'start_predicting_ms': start_predicting_ms,
							'end_ms': int(time.time() * 1000),
						}
						if 'userdata' in req:
							data['userdata'] = req['userdata']
						self.req_postprocess(data)
						callback_files = []
						json_fname = fname + '.json'
						json.dump(data, open(json_fname, 'w'), indent=4, default=str)
						get_files = [os.path.basename(gn) for gn in req.get('get_files', ['*'])]
						dbprint('get_files:', get_files)
						callback_files = list(set(chain(*[glob.glob(os.path.join(tmpdir, gn)) for gn in get_files])))
						dbprint('get_files:', callback_files)
						total_size = 0
						if callback_uri.startswith('file://'):
							ddir = os.path.expanduser(urllib.parse.urlparse(callback_uri).path)
							if not os.path.exists(ddir):
								os.makedirs(ddir)
							cmd = ['cp', '-r']
							for fname in callback_files:
								cmd.append(fname)
								total_size += os.path.getsize(fname)
							cmd.append(ddir)
						elif callback_uri.startswith('redis://'):
							self.redis_callback(callback_uri, data, callback_files if req.get('add_files_to_json', False) else None, [tmpdir])
						else:
							cmd = ['curl']
							for fname in callback_files:
								cmd +=  ['-F', 'file[]=@{};filename={}'.format(fname, os.path.basename(fname))]
								total_size += os.path.getsize(fname)
							cmd.append(callback_uri)
						if total_size > 0:
							dbprint("Total upload file size: {}".format(total_size))
							item = {'cmd': cmd}
							item['filelist'] = callback_files
							item['tmpdir'] = tmpdir
							item['debug'] = self.debug
							self.uploader_q.put(item)
					# a request processing finished
					break

	# subprocess
	def predictor(self, parent_pid, parent_q, child_q):

		process_file_dir = os.environ['PROCESS_FILE_LIB_DIR']
		sys.path.append(process_file_dir)

		i = 1
		waiting_message = True
		while psutil.pid_exists(parent_pid):
			try:
				if waiting_message:
					dbprint('Predictor waiting ({}) ...'.format(i))
				waiting_message = False
				i += 1
				item = child_q.get(True, QUEUE_TIMEOUT)
				waiting_message = True
				dbprint('item:{}'.format(item))
				if item.get('quit', False):
					break
				self.debug = item['debug']
				from process_file import process_file
				dbprint('PROCESS FILE ARGS: {}'.format(*item['args'], **item['kwds']))
				odata = process_file(*item['args'], **item['kwds'])
#				dbprint('PROCESSED DATA:', odata)
				parent_q.put(odata)
			except Empty:
				continue
			except Exception as e:
				parent_q.put({'error': str(e)})
				dbprint('Predictor exception:', e)
				traceback.print_exc(file=open(os.path.expanduser(LAST_ERROR), 'a'))
		dbprint('Predictor is exiting')

	def run(self):
		self.init()
		self.uploader_p.start()
		self.pp.start()
		while not self.finish:
			try:
				dbprint('Waiting next request ...')
				req,fnames,tmpdir = self.wait4request()
				if req:
					dbprint('Request received')
					try:
						self.process_request(req, fnames, tmpdir)
						dbprint('Request processed')
					except KeyboardInterrupt:
						raise
					except Exception as e:
						shutil.rmtree(tmpdir, ignore_errors=True)
						error_str = traceback.format_exc()
						dbprint(error_str)
						if req:
							req['error'] = error_str
							self.r.lpush(os.environ['REDIS_ERROR_QUEUE'], json.dumps(req, default=str))
			except KeyboardInterrupt:
				self.stop_processes()
				raise
			except Exception as e:
				traceback.print_exc(file=open(os.path.expanduser('~/tmp/last_error'), 'a'))
				dbprint('Run exception: {}'.format(e))
		self.stop_processes()

	def stop_processes(self):
		dbprint('Main process stopping...')
		self.uploader_q.put({'quit': True})
		self.child_q.put({'quit': True})
		self.uploader_p.join()
		self.pp.join()
		dbprint('Processes stopped')

	def terminate_gracefully(self,  signum, frame):
		self.finish = True
		dbprint('Finish initialised with signal {} at frame {}'.format(signum, frame))

if __name__ == '__main__':

	def main():

		dbprint('Started')

		pl = predictorLoop(os.environ.get('REDIS_HOST', 'localhost'))
		pl.run()
		dbprint('Finished')

	main()

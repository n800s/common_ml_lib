import numpy as np
import cv2

def merge_images(imglist, channels=3):
	rs = None
	for i,img in enumerate(imglist[:channels]):
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		if rs is None:
			rs = np.zeros((img.shape[0], img.shape[1], channels), dtype=np.uint8)
		rs[:,:,i] = img
	return rs

def getDivs(N):
	factors = {1}
	maxP  = int(N**0.5)
	p,inc = 2,1
	while p <= maxP:
		while N%p==0:
			factors.update([f*p for f in factors])
			N //= p
			maxP = int(N**0.5)
		p,inc = p+inc,2
	if N>1:
		factors.update([f*N for f in factors])
	return sorted(factors)

def find_rect_pair(v):
	numlist = getDivs(v)
	min_dist = v
	min_pair = None
	for pair in [(v//n, n) for n in numlist]:
		if abs(pair[0] - pair[1]) < min_dist:
			min_dist = abs(pair[0] - pair[1])
			min_pair = pair
	return min_pair

def video_generator(video_reader):
	frame_count = int(video_reader.get(cv2.CAP_PROP_FRAME_COUNT))
	for i in range(frame_count):
		ret_val,frame = video_reader.read()
		if not ret_val:
			break
		yield frame

def complex_frame_generator(frame_generator, frames_per_image=1, skip_frames_per_image=1, channels=3, frames2skip_after_image=0):
	rect_pair = find_rect_pair(frames_per_image)
	frame_index = 0
	while True:
		framelist = []
		for i in range(frames_per_image):
			frames2skip = 0
			img2merge = []
			while len(img2merge) < channels:
				try:
					frame = next(frame_generator)
				except StopIteration:
					break
				frame_index += 1
				if frames2skip <= 0:
					img2merge.append(frame)
					frames2skip = skip_frames_per_image
				else:
					frames2skip -= 1
			if len(img2merge) == channels:
				framelist.append(img2merge[0] if channels == 1 else merge_images(img2merge))
			else:
				break
		if len(framelist) == frames_per_image:
			row_image_list = []
			for ri in range(rect_pair[0]):
				row_image_list.append(cv2.hconcat(framelist[ri*rect_pair[1]:(ri+1) * rect_pair[1]]))
			image = cv2.vconcat(row_image_list)
			yield image,frame_index,framelist
			for j in range(frames2skip_after_image):
				try:
					next(frame_generator)
				except StopIteration:
					break
				frame_index += 1
		else:
			break

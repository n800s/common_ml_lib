#!/usr/bin/env python3

import sys, os, glob, json, time, numbers
from copy import deepcopy
import logging
from resultsmontage import ResultsMontage
from lcolors import get_color
import cv2
import numpy as np
import torch
from ultralytics import YOLO
from ultralytics.utils.plotting import Annotator
from ultralytics.utils import LOGGING_NAME
from ultralytics.data.augment import LetterBox
from PIL import ImageFont, ImageDraw, Image

USE_FFMPEG = True

if USE_FFMPEG:
	from ffmpeg_writer import FFMPEGWriter

BORDER_COLOR = (0, 0, 0)
DET_SIZE = (1028, 736)
I_EXTS = ['.jpg', '.png', '.jpeg']
V_EXTS = ['.mp4', '.avi', '.webm', '.mov']
FONTPATH = os.path.expanduser('~/.config/Ultralytics/Arial.ttf')

class Predictor:

	def __init__(self, model_path, class_color_dict):
		# Load the YOLOv8 model
		self.model = YOLO(model_path)
		self.class_color_dict = class_color_dict

	def colors(self, label, bgr):
		c = self.class_color_dict[label] if label in self.class_color_dict else get_color(label)
		if not bgr:
			c = list(reversed(c))
		return c

	def predict_img(self, img, src_frames, make_images=True, draw_boxes=True, clmap=None, max_ann=5, class_titles=None, **predict_kwds):
		rs = {
			'frame_data': {
				'frame_size': img.shape[0] * img.shape[1],
				'frame_shape': img.shape,
			},
			'js': {
				'instances': [],
				'image_size': int(img.shape[0] * img.shape[1]),
				'image_shape': img.shape,
			},
		}
		# Run YOLOv8 inference on the frame
#		print('INFER IMG SHAPE', img.shape, file=sys.stderr)
#		result = self.model.predict(img, retina_masks=True, imgsz=1280)[0]
		result = self.model.predict(img, retina_masks=True, **predict_kwds)[0]
#		result = self.model.predict(img, retina_masks=True, conf=0.15, iou=0.7, imgsz=1280)[0]
#		print('NAMES', result.names, file=sys.stderr)
#		print('BOXES', result.boxes, file=sys.stderr)
#		print('PROBS', result.probs.data, file=sys.stderr)
		names = result.names if clmap is None else {k:clmap[v] for k,v in result.names.items()}

		if make_images:
			# Visualize the results
#			rs['annotated_img'] = cv2.cvtColor(result.plot(boxes=draw_boxes, line_width=1), cv2.COLOR_RGB2BGR)
#			rs['annotated_img'] = result.plot(boxes=True, line_width=1)
			rs['annotated_img'] = self.plot(result, boxes=draw_boxes, line_width=1, clmap=clmap, class_titles=class_titles, font_size=20)
			rs['annotated_src_frames'] = [self.plot(result, boxes=draw_boxes, line_width=1, clmap=clmap, img=src_frame, max_ann=max_ann,
				class_titles=class_titles, font_size=20) for src_frame in src_frames]

		half_shape = [v//4 for v in img.shape[:2]]
#		print('HALF SHAPE', half_shape, file=sys.stderr)
		if result.boxes is None:
			# considering classification mode
			probs = result.probs.data.tolist()
			annotations = [(names[j], probs[j]) for j in range(len(names))]
			annotations.sort(key=lambda v: v[1], reverse=True)
			annotations = annotations[:5]
			rs['frame_data'] = {
				annotations[0][0]: annotations[0][1],
				'confidence': annotations,
			}
		else:
			annotations = []
			boxes = result.boxes.cpu()
			montage = ResultsMontage(half_shape, 2, boxes.shape[0]*2 if boxes else 0, depth=4)
			if boxes:
#				print('BOXES SHAPE', boxes.shape, file=sys.stderr)
				masks = None if result.masks is None else result.masks.cpu()
				if masks and 'pixels_per_image' not in rs['js']:
					rs['js']['pixels_per_image'] = {}
				for bi in range(boxes.shape[0]):
					b = boxes[bi]
#					print('BOX', b, file=sys.stderr)
					cname = names[int(b.cls)]
					box = b.xyxyn.numpy().tolist()[0]
					box[0] = int(box[0] * img.shape[1])
					box[1] = int(box[1] * img.shape[0])
					box[2] = int(box[2] * img.shape[1])
					box[3] = int(box[3] * img.shape[0])
#					print('BOX', cname, float(b.conf), list(b.xyxyn.numpy()), file=sys.stderr)
					if masks and len(masks.xy[bi]) > 0:
#						print('MASK SIZE', len(masks.xy[bi]), file=sys.stderr)
						# create bitmap of mask
						pnts = [[int(img.shape[1]*mp[0]), int(img.shape[0]*mp[1])] for mp in masks.xyn[bi]]
#						print('TYPE', type(masks.xyn[bi]), masks.xyn[bi].flatten().tolist())
						annotations.append([cname] + masks.xyn[bi].flatten().tolist())
					else:
						pnts = [[box[0], box[1]], [box[2], box[1]], [box[2], box[3]], [box[0], box[3]]]
						annotations.append([cname] + boxes[bi].xyxyn.numpy().tolist()[0])
#					print('PNTS', pnts, file=sys.stderr)
					mimg = np.zeros(img.shape[:2], dtype=np.uint8)
					pts = np.array(pnts, np.int32)
					pts = pts.reshape((-1,1,2))
					if masks:
						cv2.fillPoly(mimg, [pts], 255)
						rs['js']['pixels_per_image'][cname] = rs['js']['pixels_per_image'].get(cname, 0) + int(np.sum(mimg > 0))
						pixels = int(np.sum(mimg > 0))
						rs['js']['instances'].append({
							'class': cname,
							'pixels': pixels,
							'confidence': round(float(b.conf), 4),
							'box': box,
						})
						rs['frame_data'][cname] = rs['frame_data'].get(cname, 0) + pixels
						rs['frame_data'][cname + '_count'] = rs['frame_data'].get(cname + '_count', 0) + 1
					else:
						cv2.polylines(mimg, [pts], True, 255, 3)
						rs['js']['instances'].append({
							'class': cname,
							'confidence': round(float(b.conf), 4),
							'box': box,
						})
					if make_images:
						mimg = cv2.resize(mimg, (half_shape[1], half_shape[0]))
						cimg = cv2.resize(cv2.cvtColor(img, cv2.COLOR_BGR2BGRA), (half_shape[1], half_shape[0]))
						cimg[mimg>0] = [0, 0, 255, 128]
#						print('CIMG SHAPE', cimg.shape, file=sys.stderr)
						montage.addResult(cimg)
						timg = np.zeros([half_shape[0], half_shape[1]], dtype=np.uint8)
						title = class_titles[cname] if class_titles and cname in class_titles else cname
						title = '{} {:.4f}'.format(title, round(float(b.conf), 4))
						timg = self.plot_text(timg, (12, half_shape[0]//2), title, color=255, font_size=max(int(half_shape[1]/40), 10))
#						cv2.putText(img=timg,
#							text=title
#							org=(12, half_shape[0]//2),
#							fontFace=cv2.FONT_HERSHEY_SIMPLEX,
#							fontScale=0.5,
#							color=255,
#							thickness=1)
#						print('TIMG SHAPE', timg.shape, file=sys.stderr)
						montage.addResult(cv2.cvtColor(timg, cv2.COLOR_GRAY2BGRA))
#						mfname = os.path.join(OUTPUT_DIR, name + '_' + str(bi) + '_' + cname + '.png')
#						cv2.imwrite(mfname, mimg)
#						print('MONTAGE SHAPE', montage.montage.shape, file=sys.stderr)
			if make_images:
				rs['montage_img'] = montage.montage
		rs['annotations'] = annotations
		return rs

	def plot_text(self, img, xy, title, color, font_size):
		font = ImageFont.truetype(FONTPATH, font_size)
		img_pil = Image.fromarray(img)
		draw = ImageDraw.Draw(img_pil)
		draw.text(xy, title, font=font, fill=color)
		img = np.array(img_pil)
		return img

	def plot(
			self,
			result,
			conf=True,
			line_width=None,
			font_size=None,
			font=FONTPATH,
			pil=True,
			img=None,
			img_gpu=None,
			kpt_line=True,
			labels=True,
			boxes=True,
			masks=True,
			probs=True,
			clmap=None,
			max_ann=5,
			class_titles=None,
			show_conf=False,
			**kwargs  # deprecated args TODO: remove support in 8.2
	):
		"""
		Plots the detection results on an input RGB image. Accepts a numpy array (cv2) or a PIL Image.

		Args:
			conf (bool): Whether to plot the detection confidence score.
			line_width (float, optional): The line width of the bounding boxes. If None, it is scaled to the image size.
			font_size (float, optional): The font size of the text. If None, it is scaled to the image size.
			font (str): The font to use for the text.
			pil (bool): Whether to return the image as a PIL Image.
			img (numpy.ndarray): Plot to another image. if not, plot to original image.
			img_gpu (torch.Tensor): Normalized image in gpu with shape (1, 3, 640, 640), for faster mask plotting.
			kpt_line (bool): Whether to draw lines connecting keypoints.
			labels (bool): Whether to plot the label of bounding boxes.
			boxes (bool): Whether to plot the bounding boxes.
			masks (bool): Whether to plot the masks.
			probs (bool): Whether to plot classification probability
			# custom params
			clmap - translate class name map
			max_ann - max number of annotations

		Returns:
			(numpy.ndarray): A numpy array of the annotated image.
		"""
		# Deprecation warn TODO: remove in 8.2
		if 'show_conf' in kwargs:
			deprecation_warn('show_conf', 'conf')
			conf = kwargs['show_conf']
			assert type(conf) == bool, '`show_conf` should be of boolean type, i.e, show_conf=True/False'

		if 'line_thickness' in kwargs:
			deprecation_warn('line_thickness', 'line_width')
			line_width = kwargs['line_thickness']
			assert type(line_width) == int, '`line_width` should be of int type, i.e, line_width=3'

		names = result.names if clmap is None else {k:clmap[v] for k,v in result.names.items()}
		annotator = Annotator(deepcopy(result.orig_img if img is None else img),
							  line_width,
							  font_size,
							  font,
							  pil,
							  example=names)
		pred_boxes, show_boxes = result.boxes, boxes
		pred_masks, show_masks = result.masks, masks
		pred_probs, show_probs = result.probs, probs
		keypoints = result.keypoints
		if pred_masks and show_masks:
			if img_gpu is None:
				img = LetterBox(pred_masks.shape[1:])(image=annotator.result())
				img_gpu = torch.as_tensor(img, dtype=torch.float16, device=pred_masks.data.device).permute(
					2, 0, 1).flip(0).contiguous() / 255
			idx = pred_boxes.cls if pred_boxes else range(len(pred_masks))
			annotator.masks(pred_masks.data, colors=[self.colors(names[int(x)], True) for x in idx], im_gpu=img_gpu)

		if pred_boxes and show_boxes:
			for d in reversed(pred_boxes):
				c, conf, id = int(d.cls), float(d.conf) if conf else None, None if d.id is None else int(d.id.item())
				title = names[c]
				if class_titles and title in class_titles:
					title = class_titles[title]
				name = ('' if id is None else f'id:{id} ') + title
				if show_conf:
					label = (f'{name} {conf:.2f}' if conf else name) if labels else None
				else:
					label = name if labels else None
				annotator.box_label(d.xyxy.squeeze(), label, color=tuple(self.colors(names[c], True)))

		if pred_probs is not None and show_probs:
			pred_probs = pred_probs.data.tolist()
			annotations = [(names[j], pred_probs[j]) for j in range(len(names))]
			annotations.sort(key=lambda v: v[1], reverse=True)
			annotations = annotations[:max_ann]
			text = []
			for j in range(len(annotations)):
				title = annotations[j][0]
				if class_titles and title in class_titles:
					title = class_titles[title]
				text.append(f'{title} {annotations[j][1]:.2f}')
			text = f"{', '.join(text)}, "
			annotator.text((32, 32), text, txt_color=tuple(self.colors(names[j], True)))

		if keypoints is not None:
			for k in reversed(keypoints):
				annotator.kpts(k, result.orig_shape, kpt_line=kpt_line)

		return annotator.result()


class Processor:

	def __init__(self, output_dir, class_color_dict={}, place_model_path=None, model_path=None, gray_mode=False, place_class=None, return_instances=False):
		self.output_dir = output_dir
		self.gray_mode = gray_mode
		self.predictor_place = None
		self.predictor = None
		self.place_class = place_class
		self.return_instances = return_instances
		if not place_model_path is None:
			self.predictor_place = Predictor(place_model_path, class_color_dict)
		self.predictor = Predictor(model_path, class_color_dict)
		logging.getLogger(LOGGING_NAME).setLevel(logging.WARNING)

	def clean_dirs(self, dirs=[]):
		for p in dirs + [None]:
			if p is None:
				dp = self.output_dir
			else:
				dp = os.path.join(self.output_dir, p)
			if os.path.exists(dp):
				for ext in ['xml', 'json', 'txt', 'npy'] + I_EXTS + V_EXTS:
					for iname in glob.glob(os.path.join(dp, '*.' + ext.lstrip('.'))):
						os.unlink(iname)
			else:
				os.makedirs(dp)

	def pad_resize(self, img, width, height):
		aspect = img.shape[0]/img.shape[1]
		coef = min(float(width)/img.shape[1], float(height)/img.shape[0])
		img = cv2.resize(img, (int(img.shape[1]*coef), int(img.shape[0]*coef)))
		ih,iw = img.shape[:2]
#		print('IMG NEW SIZE', list(reversed(img.shape[:2])), file=sys.stderr)
		if width != iw or height != ih:
			padding_left_offset = (width - iw) // 2
			padding_top_offset = (height - ih) // 2
			padding_right_offset = width - iw - padding_left_offset
			padding_bottom_offset = height - ih - padding_top_offset
			img = cv2.copyMakeBorder(img,
				top=padding_top_offset,
				bottom=padding_bottom_offset,
				left=padding_left_offset,
				right=padding_right_offset,
				borderType=cv2.BORDER_CONSTANT,
				value=BORDER_COLOR
			)
#			print('BORDERS', padding_left_offset, padding_top_offset, padding_right_offset, padding_bottom_offset,
#							'OLD SHAPE', iw, ih, 'NEW SHAPE', img.shape[1], img.shape[0], file=sys.stderr)
		return img


	def process_frames(self, frame_data, show_boxes=True, isize=None, no_place=False, **predict_kwds):
		for image,src_frames in frame_data['frame_generator']:
# yolo tool seems like it works in BGR
#			image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
#			print('SHAPE', image.shape, file=sys.stderr)
			if not isize is None:
				image = cv2.resize(image, isize)
			rs = {
				'json_frame': {},
				'source_image': image,
			}

			if self.gray_mode:
				img = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
				img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
			else:
				img = image

			plresult = None
			if self.predictor_place and not no_place:
				plresult = self.predictor_place.predict_img(img, [], make_images=False, **predict_kwds)
				if plresult['js']['instances']:
					# union of all placeall boxes
					place_class_boxes = []
					for i,iobj in enumerate(plresult['js']['instances']):
						if iobj['class'] == self.place_class:
							place_class_boxes.append(iobj['box'])
					box = list(zip(*place_class_boxes))
					if box:
						rs['place_src_image_shape'] = img.shape
						rs['place_src_image_size'] = int(img.shape[0] * img.shape[1])
						rs['place_class'] = self.place_class
						rs['place_boxes'] = place_class_boxes
						box = [min(box[0]), min(box[1]), max(box[2]), max(box[3])]
						rs['place_box_union'] = box
					else:
#						print('Warning. No {} found in {}'.format(self.place_class, bname), file=sys.stderr)
						box = [0, 0, img.shape[1], img.shape[0]]
					bimg = img[box[1]:box[3], box[0]:box[2]]
					rs['frame_size'] = int(bimg.shape[0] * bimg.shape[1])
					rs['frame_shape'] = bimg.shape
					bimg = self.pad_resize(bimg, DET_SIZE[0], DET_SIZE[1])
					rs['padded_frame_shape'] = bimg.shape
					det_src_frames = [self.pad_resize(sfr[box[1]:box[3], box[0]:box[2]], DET_SIZE[0], DET_SIZE[1]) for sfr in src_frames]
					result = self.predictor.predict_img(bimg, det_src_frames, draw_boxes=show_boxes, **predict_kwds)
					rs['annotated_img'] = result['annotated_img']
					rs['place_js'] = plresult['js']
					rs['js'] = result['js']
					rs['montage_img'] = result.get('montage_img', None)
					rs['annotations'] = result['annotations']
					if result['frame_data']:
						rs['json_frame'] = result['frame_data']
						rs['json_frame']['place'] = {}
						for k in ('place_class', 'place_box_union', 'place_boxes', 'padded_frame_shape', 'place_src_image_size', 'place_src_image_shape'):
							if k in rs:
								rs['json_frame']['place'][k] = rs[k]
						rs['json_frame']['found_places'] = plresult['js']['instances']
#					else:
#						print('Warning. No detail classes found in {}'.format(bname), file=sys.stderr)
			if no_place or not self.predictor_place or not plresult['js']['instances']:
				result = self.predictor.predict_img(img, src_frames, draw_boxes=show_boxes, **predict_kwds)
				rs['annotated_img'] = result['annotated_img']
				rs['js'] = result['js']
				rs['frame_size'] = int(img.shape[0] * img.shape[1])
				rs['frame_shape'] = img.shape
				rs['montage_img'] = result.get('montage_img', None)
				rs['annotations'] = result['annotations']
				if result['frame_data']:
					rs['json_frame'] = result['frame_data']
#				else:
#					print('Warning. No detail classes found in {}'.format(bname), file=sys.stderr)
				
			rs['annotated_frames'] = result.get('annotated_src_frames', [rs['annotated_img']])
			yield rs

	def read_video_frame(self, video_reader, frames2skip=0):
		src_frames = []
		ret_val, frame = video_reader.read()
		if ret_val:
			src_frames.append(frame)
		for i in range(frames2skip):
			ret_val4skip,frame4skip = video_reader.read()
			if ret_val4skip:
				src_frames.append(frame4skip)
		return ret_val, frame, src_frames

	def make_video_generator(self, video_reader, frames_per_row=1, rows_per_image=1, frames2skip=0):
		frames_per_image = frames_per_row * rows_per_image
		while True:
			frame_src_frames = []
			if frames_per_image > 1:
				montage = None
				for i in range(frames_per_image):
					ret_val, frame, src_frames = self.read_video_frame(video_reader, frames2skip=frames2skip)
					frame_src_frames += src_frames
#					print('Read frame', ret_val, file=sys.stderr)
					if not ret_val:
						break
					if montage is None:
						montage = ResultsMontage(frame.shape[:2], frames_per_row, frames_per_image, depth=3)
					montage.addResult(frame)
					frame = montage.montage
			else:
				ret_val, frame, src_frames = self.read_video_frame(video_reader, frames2skip=frames2skip)
				frame_src_frames += src_frames
			if not ret_val:
				break
#			print('FRAME', frame.shape)
			yield frame, frame_src_frames

	# isize - (w, h) to resize input image
	def process_file(self, ifname, codec='mp4v', odname=None, ofname=None, ofname_montage=None, ofname_txt=None, ttype=None, algorithm='default',
			jsfname=None, show_boxes=True, isize=None, save_annotations=False, save_crops=False, frames_per_row=1, rows_per_image=1, frames2skip=0,
			no_place=False, **predict_kwds):
		print('File:\n%s' % ifname, file=sys.stderr)
		bname = os.path.splitext(os.path.basename(ifname))[0]
		if os.path.splitext(ifname)[-1] in V_EXTS:
			video_mode = True
			video_reader = cv2.VideoCapture(ifname)
			nb_frames = int(video_reader.get(cv2.CAP_PROP_FRAME_COUNT))
			fps = int(video_reader.get(cv2.CAP_PROP_FPS) / (frames2skip + 1))
			frame_data = {'frame_generator': self.make_video_generator(video_reader, frames2skip=frames2skip, frames_per_row=frames_per_row, rows_per_image=rows_per_image), 'fps': fps}
		else:
			video_mode = False
			frame_data = {'frame_generator': ((frame,[frame]) for frame in [cv2.imread(ifname)])}
#		print('Video mode', video_mode, file=sys.stderr)
		video_writer = None
		video_writer_montage = None
		json_data = {'frames': [], 'frames_js': [], 'frame_size': None, 'n_frames': 0, 'n_empty_frames': 0}
		suffix = ''
		if odname is None:
			odname = self.output_dir if self.output_dir[0] == '/' else os.path.join(os.path.dirname(sys.argv[0]), self.output_dir)
		if not os.path.exists(odname):
			os.makedirs(odname)
#		print('frame data', frame_data, file=sys.stderr)
		for i,result in enumerate(self.process_frames(frame_data, show_boxes=show_boxes, isize=isize, no_place=no_place, **predict_kwds)):
#			print('final frame result', i, result.keys(), file=sys.stderr)
			oframes = result['annotated_frames']
			iframe = cv2.resize(result['source_image'], (oframes[0].shape[1], oframes[0].shape[0]))
			oframe_montage = result['montage_img']

			if video_mode:
				if video_writer is None:
					if ofname is None:
						ofname = os.path.join(odname, bname + '_preview' + '.mp4')
					if ofname_montage is None:
						ofname_montage = os.path.join(odname, bname + '_montage' + '.mp4')
					frame_h,frame_w = oframes[0].shape[:2]
					if USE_FFMPEG:
						video_writer = FFMPEGWriter(ofname, frame_w, frame_h, fps)
					else:
						video_writer = cv2.VideoWriter(ofname, cv2.VideoWriter_fourcc(*codec), fps, (frame_w, frame_h))
					if ofname_montage and oframe_montage and oframe_montage.size:
						frame_h,frame_w = oframe_montage.shape[:2]
						if USE_FFMPEG:
							video_writer_montage = FFMPEGWriter(ofname_montage, frame_w, frame_h, fps)
						else:
							video_writer_montage = cv2.VideoWriter(ofname_montage, cv2.VideoWriter_fourcc(*codec), fps, (frame_w, frame_h))
				for oframe in oframes:
					video_writer.write(oframe)
				if video_writer_montage and oframe_montage.size:
					video_writer_montage.write(oframe_montage)
			else:
				oframe = oframes[0]
				if ofname is None:
					ofname = os.path.join(odname, bname + '_preview.png')
				cv2.imwrite(ofname, oframe)
				if ofname_montage is None:
					ofname_montage = os.path.join(odname, bname + '_montage.png')
#				print('MONTAGE FRAME SIZE', oframe_montage.size, file=sys.stderr)
				if not oframe_montage is None and oframe_montage.size:
					cv2.imwrite(ofname_montage, oframe_montage)
#				print('FILE TXT', ofname_txt)
				if save_annotations:
					if ofname_txt is None:
						ofname_txt = os.path.join(odname, bname + '_annotation.txt')
					with open(ofname_txt, 'wt') as tf:
						for ann in result['annotations']:
							print(' '.join([str(v) for v in ann]), file=tf)
				if save_crops:
					for ann_i,ann in enumerate(result['annotations']):
						clname = ann[0]
						ann = [float(v) for v in ann[1:]]
						xmin = int(min(ann[0::2]) * oframe.shape[1])
						xmax = int(max(ann[0::2]) * oframe.shape[1])
						ymin = int(min(ann[1::2]) * oframe.shape[0])
						ymax = int(max(ann[1::2]) * oframe.shape[0])
						cropped = oframe[ymin:ymax, ymin:ymax]
						ocfname = os.path.splitext(ofname)
						ocfname = ocfname[0] + '_crop_' + clname + '_' + str(ann_i) + ocfname[-1]
#						print('SAVE CROP', ocfname, file=sys.stderr)
						cv2.imwrite(ocfname, cropped)
			if json_data['frame_size'] is None:
				json_data['frame_size'] = result['frame_size']
				json_data['frame_shape'] = result['frame_shape']
			json_data['n_frames'] += 1
			if not result['json_frame']:
				json_data['n_empty_frames'] += 1
			json_data['frames'].append(result['json_frame'])
			frame_js = {'js': result['js']}
			if 'place_js' in result:
				frame_js['place_js'] = result['place_js']
			json_data['frames_js'].append(frame_js)
		t_prefix = 'total_'
		c_prefix = 'n_'
		for f in json_data['frames']:
			for k,v in f.items():
				if isinstance(v, numbers.Number):
					if (t_prefix + k) not in json_data:
						json_data[t_prefix + k] = 0
						if not k.endswith('_count'):
							json_data[c_prefix + k] = 0
					json_data[t_prefix + k] += v
					if v and not k.endswith('_count'):
						json_data[c_prefix + k] += 1
		if not video_writer is None:
			video_writer.release()
		if not video_writer_montage is None:
			video_writer_montage.release()
		if 'frames_js' in json_data and not self.return_instances:
			del json_data['frames_js']
		if jsfname != '':
			json.dump(json_data, open(os.path.join(odname, bname + suffix + '.json'), 'w') if jsfname is None else jsfname, indent=2, default=str)
		return json_data

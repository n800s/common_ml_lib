import cv2

def imread(fname, read_image_type):
	inp = cv2.imread(fname, read_image_type)
	if read_image_type == cv2.IMREAD_GRAYSCALE:
		inp = inp.reshape((inp.shape[0], inp.shape[1], 1))
	return inp

def imresize(img, size, interpolation=cv2.INTER_NEAREST):
	rs = cv2.resize(img, size, interpolation)
	if len(img.shape) == 3 and img.shape[2] == 1:
		rs = rs.reshape((rs.shape[0], rs.shape[1], 1))
	return rs

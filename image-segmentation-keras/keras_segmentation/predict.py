import glob
import random
import json
import os
import six

import cv2
import numpy as np
from tqdm import tqdm
from time import time

from .train import find_latest_checkpoint
from .data_utils import imread, imresize
from .data_utils.data_loader import get_image_array, get_segmentation_array,\
	DATA_LOADER_SEED, class_colors, get_pairs_from_paths
from .models.config import IMAGE_ORDERING

try:
	import tritonclient.grpc as grpcclient
	from tritonclient.utils import InferenceServerException

	class TritonModel:

		def __init__(self, model_name, owidth, oheight, url='localhost:8001'):
			self.model_name = model_name
			self.triton_client = grpcclient.InferenceServerClient(url, verbose=False)
			self.metadata = self.triton_client.get_model_metadata(self.model_name)
			self.ibatch, self.input_height, self.input_width, self.n_channels = self.metadata.inputs[0].shape
			self.output_width,self.output_height = owidth, oheight
			obatch, self.osize, self.n_classes = self.metadata.outputs[0].shape
			if len(self.metadata.inputs) != 1:
				raise Exception('Expect one model input only, %d found' % len(self.metadata.inputs))

		def predict(self, input_data):
			inputs = []
			outputs = []
			headers = {}
#			print('SHAPE !!!', input_data.shape, input_data.dtype)
			batch, row, col, ch = input_data.shape
			inputs.append(grpcclient.InferInput(self.metadata.inputs[0].name, [batch, row, col, ch], "FP32"))
			inputs[0].set_data_from_numpy(input_data)
			results = self.triton_client.infer(self.model_name, inputs, outputs=outputs, headers=headers)
			results = results.as_numpy(self.metadata.outputs[0].name)
			prediction = np.frombuffer(results, np.float32)
#			print('OSHAPE', prediction.shape)
			return [prediction]

	def model_from_triton(model_name, owidth, oheight):
		return TritonModel(model_name, owidth, oheight)

except ImportError:
	# optional triton not found
	pass

random.seed(DATA_LOADER_SEED)

def model_from_checkpoint_path(checkpoints_path, channels=3):

	from .models.all_models import model_from_name
	assert (os.path.isfile(checkpoints_path+"_config.json")
			), "Checkpoint %s not found." % (checkpoints_path+"_config.json")
	model_config = json.loads(
		open(checkpoints_path+"_config.json", "r").read())
	latest_weights = find_latest_checkpoint(checkpoints_path)
	assert (latest_weights is not None), "Checkpoint %s not found." % checkpoints_path
	print('model', model_from_name[model_config['model_class']], 'channels', channels)
	model = model_from_name[model_config['model_class']](
		model_config['n_classes'], input_height=model_config['input_height'],
		input_width=model_config['input_width'], channels=channels)
	ts = time()
	status = model.load_weights(latest_weights)
	print("loaded weights ", latest_weights, 'in', round(time() - ts, 2), 'secs')

	if status is not None:
		status.expect_partial()

	return model


def get_colored_segmentation_image(seg_arr, n_classes, colors=class_colors):
	output_height = seg_arr.shape[0]
	output_width = seg_arr.shape[1]

	seg_img = np.zeros((output_height, output_width, 3))

	for c in range(n_classes):
		if c > 0:
			seg_arr_c = seg_arr[:, :] == c
			seg_img[:, :, 0] += ((seg_arr_c)*(colors[c][0])).astype('uint8')
			seg_img[:, :, 1] += ((seg_arr_c)*(colors[c][1])).astype('uint8')
			seg_img[:, :, 2] += ((seg_arr_c)*(colors[c][2])).astype('uint8')

	return seg_img


def get_legends(class_names, colors=class_colors):

	n_classes = len(class_names)
	legend = np.zeros(((len(class_names) * 25) + 25, 125, 3),
					  dtype="uint8") + 255

	class_names_colors = enumerate(zip(class_names[:n_classes],
									   colors[:n_classes]))

	for (i, (class_name, color)) in class_names_colors:
		color = [int(c) for c in color]
		cv2.putText(legend, class_name, (5, (i * 25) + 17),
					cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0), 1)
		cv2.rectangle(legend, (100, (i * 25)), (125, (i * 25) + 25),
					  tuple(color), -1)

	return legend


def overlay_seg_image(inp_img, seg_img):
	orininal_h = inp_img.shape[0]
	orininal_w = inp_img.shape[1]
	seg_img = imresize(seg_img, (orininal_w, orininal_h), interpolation=cv2.INTER_NEAREST)

	fused_img = (inp_img/2 + seg_img/2).astype('uint8')
	return fused_img


def concat_lenends(seg_img, legend_img):

	new_h = np.maximum(seg_img.shape[0], legend_img.shape[0])
	new_w = seg_img.shape[1] + legend_img.shape[1]

	out_img = np.zeros((new_h, new_w, 3)).astype('uint8') + legend_img[0, 0, 0]

	out_img[:legend_img.shape[0], :  legend_img.shape[1]] = np.copy(legend_img)
	out_img[:seg_img.shape[0], legend_img.shape[1]:] = np.copy(seg_img)

	return out_img


def visualize_segmentation(seg_arr, inp_img=None, n_classes=None,
						   colors=class_colors, class_names=None,
						   overlay_img=False, show_legends=False,
						   prediction_width=None, prediction_height=None):

	if n_classes is None:
		n_classes = np.max(seg_arr)

	seg_img = get_colored_segmentation_image(seg_arr, n_classes, colors=colors)

	if inp_img is not None:
		original_h = inp_img.shape[0]
		original_w = inp_img.shape[1]
		seg_img = imresize(seg_img, (original_w, original_h), interpolation=cv2.INTER_NEAREST)

	if (prediction_height is not None) and (prediction_width is not None):
		seg_img = imresize(seg_img, (prediction_width, prediction_height), interpolation=cv2.INTER_NEAREST)
		if inp_img is not None:
			inp_img = imresize(inp_img, (prediction_width, prediction_height))

	if overlay_img:
		assert inp_img is not None
		seg_img = overlay_seg_image(inp_img, seg_img)

	if show_legends:
		assert class_names is not None
		legend_img = get_legends(class_names, colors=colors)
		seg_img = concat_lenends(seg_img, legend_img)

	return seg_img


def predict(model=None, inp=None, out_fname=None,
			checkpoints_path=None, overlay_img=False,
			class_names=None, show_legends=False, colors=class_colors,
			prediction_width=None, prediction_height=None,
			read_image_type=cv2.IMREAD_COLOR):

	if model is None and (checkpoints_path is not None):
		model = model_from_checkpoint_path(checkpoints_path, channels=1 if read_image_type == cv2.IMREAD_GRAYSCALE else 3)

	assert (inp is not None)
	assert ((type(inp) is np.ndarray) or isinstance(inp, six.string_types)),\
		"Input should be the CV image or the input file name"

	if isinstance(inp, six.string_types):
		inp = imread(inp, read_image_type)

	assert (len(inp.shape) == 3 or len(inp.shape) == 1 or len(inp.shape) == 4), "Image should be h,w,3 "

	output_width = model.output_width
	output_height = model.output_height
	input_width = model.input_width
	input_height = model.input_height
	n_classes = model.n_classes

	x = get_image_array(inp, input_width, input_height,
						ordering=IMAGE_ORDERING)
	pr = model.predict(np.array([x]))[0]
	pr = pr.reshape((output_height,  output_width, n_classes)).argmax(axis=2)

	seg_img = visualize_segmentation(pr, inp, n_classes=n_classes,
									 colors=colors, overlay_img=overlay_img,
									 show_legends=show_legends,
									 class_names=class_names,
									 prediction_width=prediction_width,
									 prediction_height=prediction_height)

	if out_fname is not None:
		cv2.imwrite(out_fname, seg_img)

	return pr


def predict_multiple(model=None, inps=None, inp_dir=None, out_dir=None,
					 checkpoints_path=None, overlay_img=False,
					 class_names=None, show_legends=False, colors=class_colors,
					 prediction_width=None, prediction_height=None, read_image_type=cv2.IMREAD_COLOR):

	print('read_image_type grayscale?', read_image_type == cv2.IMREAD_GRAYSCALE)
	if model is None and (checkpoints_path is not None):
		model = model_from_checkpoint_path(checkpoints_path, channels=1 if read_image_type == cv2.IMREAD_GRAYSCALE else 3)

	if inps is None and (inp_dir is not None):
		inps = glob.glob(os.path.join(inp_dir, "*.jpg")) + glob.glob(
			os.path.join(inp_dir, "*.png")) + \
			glob.glob(os.path.join(inp_dir, "*.jpeg"))
		inps = sorted(inps)

	assert type(inps) is list

	all_prs = []

	if not out_dir is None:
		if not os.path.exists(out_dir):
			os.makedirs(out_dir)


	for i, inp in enumerate(tqdm(inps)):
		if out_dir is None:
			out_fname = None
		else:
			if isinstance(inp, six.string_types):
				out_fname = os.path.join(out_dir, os.path.basename(inp))
			else:
				out_fname = os.path.join(out_dir, str(i) + ".jpg")

		pr = predict(model, inp, out_fname,
					 overlay_img=overlay_img, class_names=class_names,
					 show_legends=show_legends, colors=colors,
					 prediction_width=prediction_width,
					 prediction_height=prediction_height, read_image_type=read_image_type)

		all_prs.append(pr)

	return all_prs


def set_video(inp):
	cap = cv2.VideoCapture(inp)
	fps = int(cap.get(cv2.CAP_PROP_FPS))
	video_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
	video_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
	fcount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
	return cap, fps, fcount


def predict_video(model=None, inp=None, output=None,
				  checkpoints_path=None, display=False, overlay_img=True,
				  class_names=None, mask_class_names=None, show_legends=False, colors=class_colors,
				  prediction_width=None, prediction_height=None, codec="XVID", read_image_type=cv2.IMREAD_COLOR):

	if model is None and (checkpoints_path is not None):
		model = model_from_checkpoint_path(checkpoints_path, channels=1 if read_image_type == cv2.IMREAD_GRAYSCALE else 3)
	n_classes = model.n_classes
	if mask_class_names and class_names:
		mask_class_indexes = [class_names.index(c) for c in mask_class_names]

	cap, fps, fcount = set_video(inp)
	video = None
	video_mask = None
	t = time()
	n = 0
	while(cap.isOpened()):
		ret, iframe = cap.read()
		if iframe is not None:
			if read_image_type == cv2.IMREAD_GRAYSCALE:
				frame = cv2.cvtColor(iframe, cv2.COLOR_BGR2GRAY).reshape((iframe.shape[0], iframe.shape[1], 1))
			else:
				frame = iframe
			print('FRAME SHAPE', frame.shape, 'TYPE', frame.dtype)
			pr = predict(model=model, inp=frame)
			print(prediction_width, prediction_height, 'SHAPE', pr.shape, 'TYPE', pr.dtype, np.unique(pr))
			if read_image_type == cv2.IMREAD_GRAYSCALE:
				oframe = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
			fused_img = visualize_segmentation(
				pr, oframe, n_classes=n_classes,
				colors=colors,
				overlay_img=overlay_img,
				show_legends=show_legends,
				class_names=class_names,
				prediction_width=prediction_width,
				prediction_height=prediction_height
			)
		else:
			break
		if output is not None:
			if video is None:
				size = (fused_img.shape[1], fused_img.shape[0])
				fourcc = cv2.VideoWriter_fourcc(*codec)
				video = cv2.VideoWriter(output, fourcc, fps, size)
				if mask_class_names:
					file_shape = tuple([fcount] + list(pr.shape))
					print('FILE SHAPE', file_shape)
					mask_fname = os.path.splitext(output)[0] + '.npm'
					np_meta = np.memmap(mask_fname, dtype=int, mode='w+', shape=(3,), offset=0)
					np_meta[:3] = file_shape
					np_meta.flush()
					np_file = np.memmap(mask_fname, dtype=bool, mode='r+', shape=file_shape, offset=256)
			video.write(fused_img)
			if mask_class_names:
				np_file[n,:,:] = np.isin(pr, mask_class_indexes)
				np_file.flush()
				mask = np_file[n]
				print('NP_FILE', np.unique(np_file))
#				if video_mask is None:
#					size = (mask.shape[1], mask.shape[0])
#					fourcc = cv2.VideoWriter_fourcc(*codec)
#					video_mask = cv2.VideoWriter(os.path.splitext(output)[0] + '_mask.avi', fourcc, fps, size)
#			video_mask.write(cv2.cvtColor(mask.astype(np.uint8)*255, cv2.COLOR_GRAY2BGR))

#		if display:
#			cv2.imshow('Frame masked', fused_img)
#			if cv2.waitKey(fps) & 0xFF == ord('q'):
#				break
		n += 1
	cap.release()
	print("FPS: {}".format(n/(time() - t)))
	if output is not None:
		video.release()
	cv2.destroyAllWindows()


def evaluate(model=None, inp_images=None, annotations=None,
			 inp_images_dir=None, annotations_dir=None, checkpoints_path=None, read_image_type=cv2.IMREAD_COLOR):

	if model is None:
		assert (checkpoints_path is not None),\
				"Please provide the model or the checkpoints_path"
		model = model_from_checkpoint_path(checkpoints_path, channels=1 if read_image_type == cv2.IMREAD_GRAYSCALE else 3)

	if inp_images is None:
		assert (inp_images_dir is not None),\
				"Please provide inp_images or inp_images_dir"
		assert (annotations_dir is not None),\
			"Please provide inp_images or inp_images_dir"

		paths = get_pairs_from_paths(inp_images_dir, annotations_dir)
		paths = list(zip(*paths))
		inp_images = list(paths[0])
		annotations = list(paths[1])

	assert type(inp_images) is list
	assert type(annotations) is list

	tp = np.zeros(model.n_classes)
	fp = np.zeros(model.n_classes)
	fn = np.zeros(model.n_classes)
	n_pixels = np.zeros(model.n_classes)


	for inp, ann in tqdm(zip(inp_images, annotations)):
		pr = predict(model, inp, read_image_type=read_image_type)
		gt = get_segmentation_array(ann, model.n_classes,
									model.output_width, model.output_height,
									no_reshape=True, read_image_type=read_image_type)
		gt = gt.argmax(-1)
		pr = pr.flatten()
		gt = gt.flatten()

		for cl_i in range(model.n_classes):

			tp[cl_i] += np.sum((pr == cl_i) * (gt == cl_i))
			fp[cl_i] += np.sum((pr == cl_i) * ((gt != cl_i)))
			fn[cl_i] += np.sum((pr != cl_i) * ((gt == cl_i)))
			n_pixels[cl_i] += np.sum(gt == cl_i)

	cl_wise_score = tp / (tp + fp + fn + 0.000000000001)
	n_pixels_norm = n_pixels / np.sum(n_pixels)
	frequency_weighted_IU = np.sum(cl_wise_score*n_pixels_norm)
	mean_IU = np.mean(cl_wise_score)

	return {
		"frequency_weighted_IU": frequency_weighted_IU,
		"mean_IU": mean_IU,
		"class_wise_IU": cl_wise_score
	}
